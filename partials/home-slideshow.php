<div class="rotating-image-wrapper">
  <div class="slideshow">
    <div class="slideshow-container">
      <div>
        <img src="<?php echo get_template_directory_uri(); ?>/img/slide1.jpg" class="slideshow__slide-img" alt="">
        <div class="slideshow__slide-info row">
          <div class="columns small-9 small--centered medium-6 right-justify slideshow__slide-info--inner">
            <h1 class="slideshow__slide-info-header">James Brown</h1>
            <p class="slideshow__slide-info-subheader">CBS Sports NFL Today; Showtime-Inside the NFL</p>
            <p class="slideshow__slide-info-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, ectetur adipisicing  sed do eiusmodtempor incididunt ut labore et dolore aliqua.</p>
            <a href="#" class="button slideshow__slide-info-button">Read James's Testimony</a>
          </div>
        </div>
      </div>

      <div>
        <img src="<?php echo get_template_directory_uri(); ?>/img/slide4.jpg" class="slideshow__slide-img" alt="">
        <div class="slideshow__slide-info row">
          <div class="columns small-9 small--centered  medium-pull-5 medium-6 left-justify slideshow__slide-info--inner">
            <h1 class="slideshow__slide-info-header">Urban Meyer</h1>
            <p class="slideshow__slide-info-subheader">Head Football Coach, Ohio State University</p>
            <p class="slideshow__slide-info-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, ectetur adipisicing  sed do eiusmodtempor incididunt ut labore et dolore aliqua.</p>
            <a href="#" class="button slideshow__slide-info-button">Read Urban's Testimony</a>
          </div>
        </div>
      </div>

      <div>
        <img src="<?php echo get_template_directory_uri(); ?>/img/slide5.jpg" class="slideshow__slide-img" alt="">
        <div class="slideshow__slide-info row">
          <div class="columns small-9 small--centered right-justify  medium-6 slideshow__slide-info--inner">
            <h1 class="slideshow__slide-info-header">Mike Mularkey</h1>
            <p class="slideshow__slide-info-subheader">Head Football Coach, Tennessee Titans</p>
            <p class="slideshow__slide-info-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, ectetur adipisicing  sed do eiusmodtempor incididunt ut labore et dolore aliqua.</p>
            <a href="#" class="button slideshow__slide-info-button">Read Mike's Testimony</a>
          </div>
        </div>
      </div>

      <div>
        <img src="<?php echo get_template_directory_uri(); ?>/img/slide6.jpg" class="slideshow__slide-img" alt="">
        <div class="slideshow__slide-info row">
          <div class="columns small-9 small--centered  medium-pull-5 medium-6 left-justify medium-6 slideshow__slide-info--inner">
            <h1 class="slideshow__slide-info-header">Tony Dungy</h1>
            <p class="slideshow__slide-info-subheader">Super Bowl-winning Head Coach; NBC Sports Analyst</p>
            <p class="slideshow__slide-info-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, ectetur adipisicing  sed do eiusmodtempor incididunt ut labore et dolore aliqua.</p>
            <a href="#" class="button slideshow__slide-info-button">Read Tony's Testimony</a>
          </div>
        </div>
      </div>

      <div>
        <img src="<?php echo get_template_directory_uri(); ?>/img/slide2.jpg" class="slideshow__slide-img" alt="">
        <div class="slideshow__slide-info row">
          <div class="columns small-9  medium-6 medium-pull-5 medium-6 left-justify small--centered slideshow__slide-info--inner">
            <h1 class="slideshow__slide-info-header">Clint Hurdle</h1>
            <p class="slideshow__slide-info-subheader">Manager, Pittsburgh Pirates</p>
            <p class="slideshow__slide-info-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, ectetur adipisicing  sed do eiusmodtempor incididunt ut labore et dolore aliqua.</p>
            <a href="#" class="button slideshow__slide-info-button">Read Clint's Testimony</a>
          </div>
        </div>
      </div>

      <div>
        <img src="<?php echo get_template_directory_uri(); ?>/img/slide3.jpg" class="slideshow__slide-img" alt="">
        <div class="slideshow__slide-info row">
          <div class="columns small-9 small--centered right-justify medium-6 slideshow__slide-info--inner">
            <h1 class="slideshow__slide-info-header">Billy Donovan</h1>
            <p class="slideshow__slide-info-subheader">Head Coach, Oklahoma City Thunder (NBA)</p>
            <p class="slideshow__slide-info-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, ectetur adipisicing  sed do eiusmodtempor incididunt ut labore et dolore aliqua.</p>
            <a href="#" class="button slideshow__slide-info-button">Read Billy's Testimony</a>
          </div>
        </div>
      </div>


          <div>
            <img src="<?php echo get_template_directory_uri(); ?>/img/slide_7.jpg" class="slideshow__slide-img" alt="">
            <div class="slideshow__slide-info row">
              <div class="columns small-9 small--centered left-justify medium-pull-5 medium-6 slideshow__slide-info--inner">
                <h1 class="slideshow__slide-info-header">Clark Kellogg</h1>
                <p class="slideshow__slide-info-subheader">CBS Sports Basketball Analyst</p>
                <p class="slideshow__slide-info-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, ectetur adipisicing  sed do eiusmodtempor incididunt ut labore et dolore aliqua.</p>
                <a href="#" class="button slideshow__slide-info-button">Read Clark's Testimony</a>
              </div>
            </div>
          </div>

    </div>
  </div>
</div>
