<?php get_header(); ?>
	<main>
		<?php get_template_part( 'partials/home', 'slideshow' ); ?>
		<?php get_template_part( 'loops/loop', 'frontpage' ); ?>
	</main>
<?php get_footer(); ?>
