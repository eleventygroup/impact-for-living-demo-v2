<?php get_template_part( 'head' ); ?>
<header class="main-header">
  <div class="row">
    <div class="large-4 columns">
      <a href="<?php echo site_url(); ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/site-logo.png" alt="Impact for Living" class="main-logo" /></a>
    </div>
    <div class="large-8 columns">

      <div class="title-bar title-bar--main" data-hide-for="large">
        <button class="menu-icon" type="button"></button>
        <div class="title-bar-title">Menu</div>
      </div>


      <?php wp_nav_menu( array(
      	'theme_location'  => 'header-menu',
      	'container'       => false,
      	'menu_class'      => 'vertical medium-horizontal menu main-navigation',
      	'items_wrap'      => '<ul class="%2$s" data-responsive-menu="drilldown medium-dropdown">%3$s</ul>',
      	'depth'           => 0,
      	'walker' => new gs_walker
      	)
      );
      ?>

    </div>
  </div>
</header>
