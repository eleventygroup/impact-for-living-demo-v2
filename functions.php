<?php
remove_action('wp_head', 'rsd_link'); // remove really simple discovery link
remove_action('wp_head', 'wp_generator'); // remove wordpress version

remove_action('wp_head', 'feed_links', 2); // remove rss feed links (make sure you add them in yourself if youre using feedblitz or an rss service)
remove_action('wp_head', 'feed_links_extra', 3); // removes all extra rss feed links

remove_action('wp_head', 'index_rel_link'); // remove link to index page
remove_action('wp_head', 'wlwmanifest_link'); // remove wlwmanifest.xml (needed to support windows live writer)

remove_action('wp_head', 'start_post_rel_link', 10, 0); // remove random post link
remove_action('wp_head', 'parent_post_rel_link', 10, 0); // remove parent post link
remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0); // remove the next and previous post links
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 );

remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0 );

// REMOVE WP EMOJI
remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('wp_print_styles', 'print_emoji_styles');

remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
remove_action( 'admin_print_styles', 'print_emoji_styles' );

// remove api
remove_action( 'wp_head', 'rest_output_link_wp_head');

remove_action( 'wp_head', 'wp_oembed_add_discovery_links');

remove_action( 'template_redirect', 'rest_output_link_header', 11, 0 );

// Styles
// Register style sheet.
add_action( 'wp_enqueue_scripts', 'register_theme_styles_scripts' );

/**
 * Register style sheet.
 */
function register_theme_styles_scripts() {
	wp_register_style( 'theme-css', get_template_directory_uri().'/style.css', null, null );
	wp_enqueue_style( 'theme-css' );

  wp_deregister_script( 'jquery' );
  wp_register_script( 'jquery', includes_url( '/js/jquery/jquery.js' ), false, NULL, true );
  wp_enqueue_script( 'jquery' );

  wp_register_script( 'theme-js', get_template_directory_uri().'/js/script.js', array('jquery'), null, true );
  wp_enqueue_script( 'theme-js' );
}

// Menus
function register_my_menu() {
  register_nav_menu('header-menu',__( 'Header Menu' ));
  register_nav_menu('footer-menu',__( 'Footer Menu' ));
}
add_action( 'init', 'register_my_menu' );


// main menu add a class
function add_specific_menu_location_atts( $atts, $item, $args ) {
    // check if the item is in the primary menu
    if( $args->theme_location == 'header-menu' ) {
      // add the desired attributes:
      $atts['class'] = 'main-navigation__link';
    } else if('footer-menu') {
			// add the desired attributes:
      $atts['class'] = 'footer-navigation__link';
		}
    return $atts;
}
add_filter( 'nav_menu_link_attributes', 'add_specific_menu_location_atts', 10, 3 );


class gs_walker extends Walker_Nav_Menu {

    /*
	 * Add vertical menu class and submenu data attribute to sub menus
	 */

	function start_lvl( &$output, $depth = 0, $args = array() ) {
		$indent = str_repeat("\t", $depth);
		$output .= "\n$indent<ul id=\"example-menu\" class=\"top-bar vertical menu\">\n";
	}
}
