<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>


  <?php
  // check if the repeater field has rows of data
  if( have_rows('section') ):
   	// loop through the rows of data
      while ( have_rows('section') ) : the_row();
          // display a sub field value ?>
          <div class="<?php the_sub_field('css_class'); ?>">
            <section class="row">
              <article class="columns">
                <?php the_sub_field('textarea'); ?>
              </article>
            </section>
          </div>
    <?php endwhile;
  else :
      // no rows found
  endif;
  ?>

<?php endwhile; else : ?>
  <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
<?php endif; ?>
