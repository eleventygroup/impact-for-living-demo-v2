<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
  <section class="row">
    <article class="columns medium-8 medium-centered">
      <h1><?php the_title(); ?></h1>
      <?php the_content(); ?>
    </article>
  </section>
<?php endwhile; else : ?>
  <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
<?php endif; ?>
