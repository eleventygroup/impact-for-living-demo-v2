<footer class="main-footer">
  <div class="row">
    <div class="large-8 large-push-4 columns">
      <?php wp_nav_menu( array(
      	'theme_location'  => 'footer-menu',
      	'container'       => 'nav',
      	'menu_class'      => 'footer-navigation medium-horizontal menu'
      	)
      );
      ?>
    </div>
    <div class="large-4 large-pull-8 columns copyright">
      <p>&copy; <?php echo Date('Y'); ?> Impact for Living. All Rights Reserved.</p>
    </div>
  </div>
</footer>

<?php wp_footer(); ?>

</body>
</html>
